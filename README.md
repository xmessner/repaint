# RePAINT!
## Présentation

RePAINT! est un outil de dessins imitant certains ordinateurs 8/16 bits des années 70/80/90 avec leurs contraintes et spécificités. Vous pouvez alors ressentir le plaisir de devoir calculer au mieux l'agencement des pixels entre eux, où la limitation dans le choix des couleurs produit des images colorées et très artistiques.

Ecrit en Processing et encore en phase Alpha il permet dès à présent de dessiner, charger et exporter des dessins dans différents formats natifs pouvant être relus sur les machines d'origine.

## 16 Modes supportés

|Machines|Load|Save|Format|Etat|
| :---|:---:|:---:|:---:|:---:|
|MO5|Oui|Oui|Mémoire|FULL|
|TO7|Oui|Oui|Mémoire|FULL|
|TO7 70|Oui|Oui|Mémoire|FULL|
|TO8 BM16|Oui|Oui|Mémoire|FULL|
|TMS9928 Mode2|Oui|Oui|SC2|FULL|
|Atari Low ST/STE|Oui|Oui|PI1|FULL|
|Atari Low ST/STE + Rasters|Oui|Oui|PI1|DEV|
|Atari Medium ST/STE|Oui|Oui|PI1|FULL|
|Atari High ST/STE|Oui|Oui|PI1|FULL|
|ZX Spectrum|Oui|Oui|SCR|FULL|
|Amstrad Mode 0|Oui|Oui|SCR|FULL|
|C64 Hires|Oui|Oui|ART|FULL|
|C64 MultiColor|Oui|Oui|Koala|FULL|
|CGA Medium Resolution|Oui|Oui|BSAVE|FULL|
|Oric HiRes|Oui|Non|Mémoire|FULL|
|Spectrum 512|Oui|Non|SPU|DEV|

## Modes prévus

- EGA
- TO8 BM4
- CGA NTSC et autres résolutions
- Atari overscan
- Amstrad overscan

### A etudier si pas trop complexe à implémenter

- Apple II
- Certains modes de l'Amiga
- Gamme Atari 400/800.../XL/XE
- Consoles: Master System, Megadrive, GameBoy, PCEngine, Lynx ...

## Configuration

Le fichier de configuration est au format Json. **Machine** peut prendre les valeurs suivantes

- Pencil2
- TO770
- TO7
- MO5
- BM16
- AtariLow
- AtariMedium
- AtariHigh
- AtariRasters
- Spectrum 512
- C64HiRes
- C64MultiColor
- AmstradCPC464Mode0
- Spectrum
- OricHiRes
- CGAMedium

Si une autre valeur est rentrée, un driver de base sera utilisé. Il est non finalisé et risque d'avoir des comportements non prévus.

## L'écran

L'écran se divise en 3 zones.
- La zone graphique où on peut dessiner
- La palette de couleur
- La barre d'outils

## Clavier

### Commun
- F1: Chargement d'une image au format du driver actif
- F2: Sauvegarde d'une image au format du driver actif
- F4: Tooltips affichant des informations utile suivant les modes
- ESC: Quitte le dessin actif (ligne brisée par exemple)

### Spécifique
Suivant le mode sélectionné, d'autres raccourcis peuvent être utilisés. Toutes les modifications se font en live sur le dessin permettant d'affiner le rendu.

#### C64 MultiColor

- b : Change la couleur de fond de l'image par la couleur sélectionnée
- B : Fait clignoter le bloc courrant (désactivé dans le code)

#### CGA Medium

- b / B : Change la couleur de fond parmi les 16 disponibles
- p / P : Cycle sur une autre palette

#### Atari ST / STE - Low & Medium

- rR / gG / bB : Incrémente ou décrémente d'un indice la composante de la couleur sélectionnée
- m : Change le mode de palette entre ST / STE (7 ou F)

#### Atari ST / STE Low + rasters

- rR / gG / bB : Incrémente ou décrémente d'un indice la composante de la couleur sélectionnée
- m : Change le mode de palette entre ST / STE (7 ou F)
- a : Affiche les changements de palettes

#### TO8 BM16

- rR / gG / bB : Incrémente ou décrémente d'un indice la composante de la couleur sélectionnée

#### Amstrad Mode 0

- +/- : Change la couleur de la couleur sélectionnée parmi les 27 disponibles

## Souris

- Le bouton gauche est utilisé pour dessiner avec la couleur du crayon
- Le bouton droit est utilisé pour dessiner avec la couleur de fond
- Le bouton du milieu déplace le dessin dans la fenêtre
- La molette va gérer le niveau de zoom lorsque l'outil est sélectionné
- La molette pourra également permettre de modifier la taille de l'outil **spray**. Molette vers le haut, pour faire grossir le spay, molette vers le bas pour diminuer la taille.

Lorsqu'une couleur est sélectionnée, une petite marque indique le choix. La marque du haut montre la couleur du crayon, alors que la marque du base indique l'utilisation pour la couleur de fond.

## Outils

Les outils suivants sont disponibles et fonctionnels

- Dessin par points
- Dessin continu
- Spray
- Grille
- Ligne simple
- Ligne en étoile
- Ligne brisée
- Rectangle
- Rectanble plein
- Loupe
- Undo/Redo (1 seul niveau)
- Clear

## Bugs connus

- La dernière sélection entre couleur de fond ou de forme revient à la coueleur de forme lorsqu'un outil est sélectionné. Il faut donc systématiquement choisir à nouveau la couleur qu'on souhaite utiliser.

## Roadmap non exhaustive

- Ajouter de nouveaux outils pour le dessin
- Revoir le GUI pour prévoir l'arrivée de nouveaux modes graphiques
- Revoir la gestion de la palette pour les modes graphiques plus exigents
- Ajouter d'autres modes de sauvegardes (format graphique, sources, exécutables)
- Préparer le code pour intégrer de nouveaux modules, comme les sprites, les tiles, les layers ...
- Un mode preview du dessin avec shader
- Etendre la capacité de dessin pour d'autres modes plus exotiques, comme l'ascii ou autres.
- Calque: Permettra de faire du rotoscoping
- Copie de bloc: suivant les modes graphiques on pourra également choisir de ne copier que les pixels, que les attributs ou les 2.
- Travaille par plans sur Atari si cela apporte un plus lors de la création
- Animation de palette sur Atari
- Attribut qui flash suivant les modes qui le supportent
- Changement de la couleur de fond/écriture pour les modes avec contraintes
- Brosses à taille variable
- Gomme ayant un fonctionnement différent suivant les modes, comme par exemple supprimer uniquement les couleurs ou que les pixels.
- Dessin avec un mode mirroir à n'importe quel position de l'écran
- Detection des tuiles identiques lors de la sauvegarde pour les modes le supportant
- Plusieurs dessins à la fois dans différents modes.
- Preview des images sous forme de shader CRT

